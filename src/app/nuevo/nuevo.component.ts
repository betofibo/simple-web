import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../app.service';

@Component({
    selector: 'app-nuevo',
    templateUrl: './nuevo.component.html'
  })
  export class NuevoComponent {
    articulo: IArticulo = {
        clave: '',
        precio: 100,
        descripcion: ''
    };

    constructor(
        private service: AppService,
        private router: Router) {}

    onSubmit() {
        this.service.Nuevo(this.articulo)
        .then(result => {
            console.log('result', result);
            this.router.navigate(['/listado']);
        })
        .catch(err => console.error('nuevo', err));
    }
}
