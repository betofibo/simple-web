import { OnInit, Component } from '@angular/core';
import { AppService } from '../app.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-detalle',
    templateUrl: './detalle.component.html'
  })
  export class DetalleComponent implements OnInit {
    articulo: IArticulo = {
        clave: '',
        precio: 100,
        descripcion: ''
    };

      constructor(
        private service: AppService,
        private route: ActivatedRoute,
        private router: Router) {}

      ngOnInit() {
        const url = this.route.snapshot.url.join().split(',');
        this.Detalle(url[1]);
      }

      Detalle(clave) {
          this.service.Detalle(clave)
              .then(result => {
                  console.log('Detalle', result);
                  this.articulo = result;
              })
              .catch(err => console.error(err));
      }

      onSubmit() {
          console.log('Articulo submited', this.articulo);
          this.service.Actualiza(this.articulo.clave.trim(), this.articulo)
              .then(result => {
                  console.log(result);
                  this.router.navigate(['/listado']);
              })
              .catch(err => {
                 console.error(err)
                 this.router.navigate(['/listado']);
              });
      }

      Elimina(clave:string) {
          this.service.Elimina(clave.trim())
          .then(result =>  {
              console.log('Elimina', result);
              this.router.navigate(['/listado']);
          })
          .catch(err => {
            console.error('elimina', err);
            this.router.navigate(['/listado']);
          });
      }
  }
