import { Component, OnInit } from "@angular/core";
import { AppService } from '../app.service';

@Component({
 selector: 'listado-existencia',
 templateUrl: './listado-existencia.component.html'
})
export class ListadoExistenciaComponent implements OnInit
{
    exitenciaArticulos: IExitenciaArticulos[] = [];
    
    constructor(private service: AppService) {

    }

    ngOnInit(){
        this.ObtenerListadoArticulos();
    }

    ObtenerListadoArticulos() {
        this.service.ListadoExitencias()
        .then(result => {
            this.exitenciaArticulos = result;
        });
    }
}