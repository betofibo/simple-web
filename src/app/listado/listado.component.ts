import { OnInit, Component } from '@angular/core';
import { AppService } from '../app.service';

@Component({
    selector: 'app-listado',
    templateUrl: './listado.component.html'
  })
  export class ListaComponent implements OnInit {
      articulos: IArticulo[] =  [];

      constructor(private service: AppService) {}

      ngOnInit() {
        this.Articulos();
      }

      Articulos() {
          this.service.Listado()
              .then(result => {
                  console.log('Articulos', result);
                  this.articulos = result;
              })
              .catch((err) => console.log(err));
      }
  }
