interface IArticulo {
    clave: string;
    descripcion: string;
    precio: number;
}
