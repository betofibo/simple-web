interface IExitenciaArticulos {
    id: number;
    articuloid: number;
    almacenid: number;
    minimo: number;
    maximo: number;
    reorden: number;
    estante: string;
    total: number;
    entradas: number;
    salidas: number;
    activo: boolean;
}