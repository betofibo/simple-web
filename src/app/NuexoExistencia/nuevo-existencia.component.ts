import { Component, OnInit } from "@angular/core";
import { AppService } from '../app.service';
import { Router } from '@angular/router';

@Component({
    selector: 'nuevo-existencia',
    templateUrl: './nuevo-existencia.component.html'
})
export class NuevoExistenciaComponent implements OnInit
{
    nuevo: IExitenciaArticulos =  {
        id: 0,
        articuloid: 1,
        almacenid: 2,
        minimo: 0,
        maximo: 0,
        reorden: 0,
        estante: '',
        total: 0,
        entradas: 0,
        salidas: 0,
        activo: true
    };

    constructor(private service: AppService, private router: Router){
    }

    ngOnInit(): void {
    }

    onSubmit(): void {
        console.log(this.nuevo);
        this.service.AgregaExistencia(this.nuevo)
        .then(() => this.router.navigate(['/listado']))
        .catch((error) => console.error("Error agregando un registro", error))
    }
}