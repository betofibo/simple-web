import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ListaComponent } from './listado/listado.component';
import { DetalleComponent } from './Detalle/detalle.component';
import { NuevoComponent } from './nuevo/nuevo.component';

import { AppService } from './app.service';
import { ListadoExistenciaComponent } from './ListadoExitenciaArticulo/listado-existencia.component';
import { NuevoExistenciaComponent } from './NuexoExistencia/nuevo-existencia.component';

const routes: Routes = [
{ path: 'listado', component: ListadoExistenciaComponent },
{ path: 'nuevo', component: NuevoExistenciaComponent }
  // { path: 'detalle/:clave', component: DetalleComponent },
  // { path: 'nuevo', component:  NuevoComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    ListaComponent,
    ListadoExistenciaComponent,
    NuevoExistenciaComponent,
    DetalleComponent,
    NuevoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    AppService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
