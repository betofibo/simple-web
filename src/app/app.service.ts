import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type' : 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private readonly serviceURL =  'http://157.245.176.122/api/articulo';

  constructor(private http: HttpClient) { }

  Listado(): Promise<Array<IArticulo>> {
    return this.http.get<Array<IArticulo>>(`${this.serviceURL}/lista`).toPromise();
  }

  Detalle(clave: string): Promise<IArticulo> {
    return this.http.get<IArticulo>(`${this.serviceURL}/${clave}`).toPromise();
  }

  Actualiza(clave: string, articulo: IArticulo): Promise<any> {
    return this.http.put<any>(`${this.serviceURL}/${clave}/actualiza`, articulo, httpOptions).toPromise();
  }

  Elimina(clave): Promise<any> {
    return this.http.delete<any>(`${this.serviceURL}/${clave}/elimina`, httpOptions).toPromise();
  }

  Nuevo(articulo: IArticulo): Promise<any> { 
    return this.http.post<any>(`${this.serviceURL}/nuevo`, articulo, httpOptions).toPromise();
  }

  ListadoExitencias(): Promise<Array<IExitenciaArticulos>> {
    return this.http.get<Array<IExitenciaArticulos>>(`${this.serviceURL}/existencia`).toPromise();
  }

  AgregaExistencia(exitencia: IExitenciaArticulos): Promise<any> {
    return this.http.post<any>(`${this.serviceURL}/agregaexistencia`, exitencia, httpOptions).toPromise();
  }
}
